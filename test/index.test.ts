/* eslint-disable @typescript-eslint/ban-ts-comment */
import { expectTypeOf } from "expect-type"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { match, matchS, matchSI } from "../src/index"

describe("match", () => {
  type Matchable =
    | {
        tag: "foo"
      }
    | {
        tag: "bar"
        contents: number
      }
    | {
        tag: "baz"
        contents: string
      }

  const barContents = 2
  const bazContents = "3"

  const getMatchable = (tag: Matchable["tag"]): Matchable => {
    switch (tag) {
      case "foo":
        return { tag: "foo" }
      case "bar":
        return { tag: "bar", contents: barContents }
      case "baz":
        return { tag: "baz", contents: bazContents }
    }
  }

  describe("direct", () => {
    it("should match on tag in pipe", () => {
      const expected = "yes"
      const actual = pipe(
        getMatchable("foo"),
        match<Matchable, string>({
          foo: (_) => expected,
          bar: (n) => n.contents.toFixed().toString(),
          baz: (s) => s.contents,
        }),
      )
      expect(actual).toEqual(expected)
      // typescript infers unknown :(
      // expectTypeOf(actual).toEqualTypeOf<string>()
    })

    it("should require all tags", () => {
      pipe(
        getMatchable("foo"),
        match<Matchable, string>(
          // @ts-expect-error
          {
            foo: (_) => "yes",
            baz: (s) => s.contents,
          },
        ),
      )
    })
  })

  describe("match.i", () => {
    it("should match on tag directly", () => {
      const expected = barContents.toString()
      const actual = match.i(getMatchable("bar"))({
        foo: (_) => "yes",
        bar: (n) => n.contents.toFixed().toString(),
        baz: (s) => s.contents,
      })
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<string>()
    })

    it("should require all tags", () => {
      match.i(getMatchable("foo"))(
        // @ts-expect-error
        {
          foo: (_) => "yes",
          baz: (s) => s.contents,
        },
      )
    })
  })

  describe("match.p", () => {
    it("should partially match on tag in pipe", () => {
      const expected = "yes"
      const actual = pipe(
        getMatchable("foo"),
        match.p({
          foo: (_) => expected,
          baz: (s) => s.contents,
        }),
      )
      expect(actual).toEqual(O.some(expected))
      // typescript infers O.Option<unknown> :(
      // expectTypeOf(actual).toEqualTypeOf<O.Option<string>>()
    })

    it("should partially match on tag in pipe with fallback", () => {
      const expected = "no"
      const actual = pipe(
        getMatchable("bar"),
        match.p(
          {
            foo: (_) => "yes",
            baz: (s) => s.contents,
          },
          () => expected,
        ),
      )
      expect(actual).toEqual(expected)
      // with some help, inference works!
      expectTypeOf(actual).toEqualTypeOf<string>()
    })

    it("should require a consistent type with fallback", () => {
      pipe(
        getMatchable("foo"),
        match.p(
          {
            // @ts-expect-error
            foo: (_) => "yes",
          },
          () => 3,
        ),
      )
    })
  })

  describe("match.w", () => {
    it("should match on a tag in a pipe, and widen the type to whatever is returned", () => {
      const expected = barContents
      const actual = pipe(
        getMatchable("bar"),
        match.w({
          foo: (_) => true,
          bar: (s) => s.contents,
          baz: (s) => s.contents,
        }),
      )
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<boolean | number | string>()
    })

    it("should require all tags", () => {
      pipe(
        getMatchable("foo"),
        match.w(
          // @ts-expect-error
          {
            foo: (_) => "yes",
            bar: (s) => s.contents,
          },
        ),
      )
    })
  })

  describe("match.ip", () => {
    it("should partially match on tag, inverted", () => {
      const expected = "yes"
      const actual = match.ip(getMatchable("foo"))({
        foo: (_) => expected,
        baz: (s) => s.contents,
      })
      expect(actual).toEqual(O.some(expected))
      expectTypeOf(actual).toEqualTypeOf<O.Option<string>>()
    })

    it("should partially match on tag, inverted, with fallback", () => {
      const expected = "no"
      const actual = match.ip(getMatchable("bar"))(
        {
          foo: (_) => "yes",
          baz: (s) => s.contents,
        },
        () => expected,
      )
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<string>()
    })

    it("should require a consistent type with fallback", () => {
      // because inverted has better type inference, the error shows up on the
      // fallback in this case.
      match.ip(getMatchable("foo"))(
        {
          foo: (_) => "yes",
        },
        // @ts-expect-error
        () => 3,
      )
    })
  })

  describe("match.iw", () => {
    it("should match on tag, inverted, widening type", () => {
      const expected = true
      const actual = match.iw(getMatchable("foo"))({
        foo: (_) => expected,
        bar: (s) => s.contents,
        baz: (s) => s.contents,
      })
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<boolean | number | string>()
    })

    it("should require all tags", () => {
      match.iw(getMatchable("foo"))(
        // @ts-expect-error
        {
          foo: (_) => "yes",
          bar: (s) => s.contents,
        },
      )
    })
  })

  describe("match.pw", () => {
    it("should partially match on tag, widening type", () => {
      const expected = "yes"
      const actual = pipe(
        getMatchable("foo"),
        match.pw({
          foo: (_) => expected,
          bar: (s) => s.contents,
        }),
      )
      expect(actual).toEqual(O.some(expected))
      expectTypeOf(actual).toEqualTypeOf<O.Option<number | string>>()
    })

    it("should partially match on tag, widening type, with fallback", () => {
      const expected = true
      const actual = pipe(
        getMatchable("baz"),
        match.pw(
          {
            foo: (_) => "yes",
            bar: (s) => s.contents,
          },
          () => expected,
        ),
      )
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<boolean | number | string>()
    })
  })

  describe("match.ipw", () => {
    it("should partially match on tag, widening type, inverted", () => {
      const expected = true
      const actual = match.ipw(getMatchable("foo"))({
        foo: (_) => expected,
        baz: (s) => s.contents,
      })
      expect(actual).toEqual(O.some(expected))
      expectTypeOf(actual).toEqualTypeOf<O.Option<boolean | string>>()
    })

    it("should partially match on tag, widening type, inverted, with fallback", () => {
      const expected = true
      const actual = match.ipw(getMatchable("foo"))(
        {
          foo: (_) => expected,
          baz: (s) => s.contents,
        },
        () => 3,
      )
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<boolean | number | string>()
    })
  })

  describe("match.on", () => {
    type Action =
      | {
          type: "foo"
        }
      | {
          type: "bar"
          payload: number
        }
      | {
          type: "baz"
          payload: string
        }

    const getAction = (type: Action["type"]): Action => {
      switch (type) {
        case "foo":
          return { type: "foo" }
        case "bar":
          return { type: "bar", payload: barContents }
        case "baz":
          return { type: "baz", payload: bazContents }
      }
    }

    it("allows matching on a different discriminant key", () => {
      const expected = barContents.toString()
      const actual = match.on("type").i(getAction("bar"))({
        foo: (_) => "foo",
        bar: ({ payload }) => payload.toString(10),
        baz: ({ payload }) => payload,
      })
      expect(actual).toEqual(expected)
      expectTypeOf(actual).toEqualTypeOf<string>()
    })

    it("has all the same variants", () => {
      expect(match.on("type").i).toBeDefined()
      expect(match.on("type").p).toBeDefined()
      expect(match.on("type").w).toBeDefined()
      expect(match.on("type").ip).toBeDefined()
      expect(match.on("type").iw).toBeDefined()
      expect(match.on("type").pw).toBeDefined()
      expect(match.on("type").ipw).toBeDefined()
    })
  })

  describe("matchS", () => {
    it("should match on string unions", () => {
      type SU = "foo" | "bar" | "baz"
      const su: SU = "foo" as SU
      const expected = "abc"
      const actual = matchS(su)({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
      })
      expect(actual).toEqual(expected)
    })

    it("should match on string enums", () => {
      enum SU {
        foo = "foo",
        bar = "bar",
        baz = "baz",
      }
      const su: SU = SU.foo as SU
      const expected = "abc"
      const actual = matchS(su)({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
      })
      expect(actual).toEqual(expected)
    })

    it("should error if missing or extra matches", () => {
      type SU = "foo" | "bar" | "baz"
      const su: SU = "foo" as SU
      matchS(su)(
        // @ts-expect-error
        {
          foo: () => "abc",
          baz: () => "ghi",
        },
      )
      matchS(su)({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
        // @ts-expect-error
        quux: () => "jkl",
      })
    })
  })

  describe("matchSI", () => {
    it("should match on string unions", () => {
      type SU = "foo" | "bar" | "baz"
      const su: SU = "foo" as SU
      const expected = "abc"
      const actual = matchSI({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
      })(su)
      expect(actual).toEqual(expected)
    })

    it("should match on string enums", () => {
      enum SU {
        foo = "foo",
        bar = "bar",
        baz = "baz",
      }
      const su: SU = SU.foo as SU
      const expected = "abc"
      const actual = matchSI({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
      })(su)
      expect(actual).toEqual(expected)
    })

    it("should error if missing matches", () => {
      type SU = "foo" | "bar" | "baz"
      const su: SU = "foo" as SU
      matchSI(
        {
          foo: () => "abc",
          baz: () => "ghi",
        },
        // @ts-expect-error
      )(su)
    })

    // current implementation of `matchSI` doesn't error on extra matches like `matchS`
    // though the worst that happens is we have extra unused code lying around.
    it("should error if extra matches", () => {
      type SU = "foo" | "bar" | "baz"
      const su: SU = "foo" as SU

      /* // Expects to have a type error here if `matchSI` supports this case.
      matchSI({
        foo: () => 'abc',
        bar: () => 'def',
        baz: () => 'ghi',
        quux: () => 'jkl',
        // @ts-expect-error
      })(su)
      */

      // have to add type parameters to get type check error.
      matchSI<SU, string>({
        foo: () => "abc",
        bar: () => "def",
        baz: () => "ghi",
        // @ts-expect-error
        quux: () => "jkl",
      })(su)
    })
  })

  describe("match.extract", () => {
    it("should extract items with matching tags", () => {
      const expected = { tag: "bar" as const, contents: barContents }
      const actual = pipe(getMatchable("bar"), match.extract("bar"))

      expect(actual).toEqual(O.some(expected))
    })

    it("should potentially match multiple tags", () => {
      const expected = { tag: "foo" as const }
      const actual = pipe(getMatchable("foo"), match.extract("bar", "foo"))

      expect(actual).toEqual(O.some(expected))

      expectTypeOf(actual).toEqualTypeOf<
        O.Option<
          | {
              tag: "foo"
            }
          | {
              tag: "bar"
              contents: number
            }
        >
      >()
    })

    it("should return none if no matches are found", () => {
      const actual = pipe(getMatchable("baz"), match.extract("bar", "foo"))

      expect(actual).toEqual(O.none)

      expectTypeOf(actual).toEqualTypeOf<
        O.Option<
          | {
              tag: "foo"
            }
          | {
              tag: "bar"
              contents: number
            }
        >
      >()
    })

    it("should fail type-checking if invalid tags are used", () => {
      pipe(
        getMatchable("baz"),
        // @ts-expect-error
        match.extract("bar", "foo", "quuux"),
      )
    })
  })
})
