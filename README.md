# `matchers`

`matchers` provides a set of functions for matching on discriminated unions in TypeScript.
