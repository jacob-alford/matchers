import type { Exact, TaggedUnionMember } from "typelevel-ts"

import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"

/**
 * Given an object type, represents a strict subset of the key/value pairs of
 * that object.
 */
export type StrictPartial<T> = keyof T extends infer K
  ? K extends string | symbol | number
    ? Partial<Omit<T, K>> & { [Key in K]?: undefined }
    : never
  : never

/**
 * Given an object type, returns only the keys which have defined properties
 */
export type DefinedKeysOf<T> = keyof T extends infer K
  ? K extends keyof T
    ? T[K] extends undefined
      ? never
      : K
    : never
  : never

type TaggedOnKey<K extends string> = { [k in K]: string }

/**
 * An object whose properties are the values of a tagged union's discriminant,
 * and whose values are functions taking the corresponding tagged union member
 * and returning an `Out`.
 */
type Matchers<Key extends string, TaggedUnion extends TaggedOnKey<Key>, Out> = {
  [D in TaggedUnion[Key]]: (v: TaggedUnionMember<TaggedUnion, Key, D>) => Out
}

type PropReturns<T> = ReturnType<((...args: Array<never>) => unknown) & T[DefinedKeysOf<T>]>

interface PartialMatch<K extends string> {
  <TU extends TaggedOnKey<K>, Z>(matchObj: StrictPartial<Matchers<K, TU, Z>>, otherwise: () => Z): (
    tu: TU,
  ) => Z
  <TU extends TaggedOnKey<K>, Z>(matchObj: StrictPartial<Matchers<K, TU, Z>>): (
    tu: TU,
  ) => O.Option<Z>
}

interface InvertedPartialMatch<K extends string, TU extends TaggedOnKey<K>> {
  <Z>(matchObj: StrictPartial<Matchers<K, TU, Z>>, otherwise: () => Z): Z
  <Z>(matchObj: StrictPartial<Matchers<K, TU, Z>>): O.Option<Z>
}
interface PartialWideningMatch<K extends string> {
  <TU extends TaggedOnKey<K>, Z, Ms extends StrictPartial<Matchers<K, TU, unknown>>>(
    matchObj: Ms,
    otherwise: () => Z,
  ): (tu: TU) => Z | PropReturns<Ms>
  <TU extends TaggedOnKey<K>, Ms extends StrictPartial<Matchers<K, TU, unknown>>>(matchObj: Ms): (
    tu: TU,
  ) => O.Option<PropReturns<Ms>>
}
interface InvertedPartialWideningMatch<K extends string, TU extends TaggedOnKey<K>> {
  <Z, Ms extends StrictPartial<Matchers<K, TU, unknown>>>(matchObj: Ms, otherwise: () => Z):
    | Z
    | PropReturns<Ms>
  <Ms extends StrictPartial<Matchers<K, TU, unknown>>>(matchObj: Ms): O.Option<PropReturns<Ms>>
}

export interface Match<K extends string> {
  /**
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * pipe(
   *   foo,
   *   match<{ tag: 'bar' } | { tag: 'baz' }, number>({
   *     bar: () => 1,
   *     baz: () => 2,
   *   }),
   * )
   */
  <TU extends TaggedOnKey<K> = never, Z = never>(matchObj: Matchers<K, TU, Z>): (tu: TU) => Z

  /**
   * Inverted argument order
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * match.i(foo)({
   *   bar: () => 1,
   *   baz: () => 2,
   * })
   */
  i: <TU extends TaggedOnKey<K>>(tu: TU) => <Z>(matchObj: Matchers<K, TU, Z>) => Z
  /**
   * Partial matching
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * pipe(
   *   foo,
   *   match.p(
   *     {
   *       bar: () => 1,
   *     },
   *     () => 2,
   *   ),
   * ) // :: number
   *
   * pipe(
   *   foo,
   *   match.p(
   *     {
   *       bar: () => 1,
   *     },
   *   ),
   * ) // :: Option<number>
   */
  p: PartialMatch<K>
  /**
   * Widening
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * pipe(
   *   foo,
   *   match.w({
   *     () => 3,
   *     () => 'three',
   *   }),
   *   // :: number | string
   * )
   */
  w: <TU extends TaggedOnKey<K>, Ms extends Exact<Matchers<K, TU, unknown>, Ms>>(
    matchObj: Ms,
  ) => (tu: TU) => PropReturns<Ms>

  /**
   * Inverted, partial
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * match.ip(foo)(
   *   {
   *     bar: () => 1,
   *   },
   *   () => 5,
   * ) // :: number
   *
   * match.ip(foo)({
   *   bar: () => 1,
   * }) // :: Option<number>
   */
  ip: <TU extends TaggedOnKey<K>>(tu: TU) => InvertedPartialMatch<K, TU>

  /**
   * Inverted, widening
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' }
   * match.iw(foo)({
   *   bar: () => 1,
   *   baz: () => 'baz',
   * }) // :: number | string
   */
  iw: <TU extends TaggedOnKey<K>>(
    tu: TU,
  ) => <Z, Ms extends Exact<Matchers<K, TU, Z>, Ms>>(matchObj: Ms) => PropReturns<Ms>

  /**
   * Partial, widening
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' } | { tag: 'quux' }
   * pipe(
   *   foo,
   *   match.pw(
   *     {
   *       bar: () => 1,
   *       baz: () => '2',
   *     },
   *     () => false,
   *   ),
   * ) // :: number | string | boolean
   *
   * pipe(
   *   foo,
   *   match.pw({
   *     bar: () => 1,
   *     baz: () => '2',
   *   }),
   * ) // :: Option<number | string>
   */
  pw: PartialWideningMatch<K>

  /**
   * Inverted, partial, widening
   * @example
   * declare const foo: { tag: 'bar' } | { tag: 'baz' } | { tag: 'quux' }
   * match.ipw(foo)(
   *   {
   *     bar: () => 1,
   *     baz: () => '2',
   *   },
   *   () => false
   * ) // :: number | string | boolean
   *
   * match.ipw(foo)({
   *   bar: () => 1,
   *   baz: () => '2',
   * }) // :: Option<number | string>
   */
  ipw: <TU extends TaggedOnKey<K>>(tu: TU) => InvertedPartialWideningMatch<K, TU>

  /**
   * Extract certain members of an ADT, narrowing the type
   * Will return Some if the value is matched, None if not matched
   *
   * @param kus A list of tags to extract members of an ADT from
   * @returns `Some` if the tag of the ADT matches a tag in `kus`,
   *          `None` if it doesn't match
   *
   * @example
   * declare const foo:
   *   | { tag: 'bar',   bar: number }
   *   | { tag: 'baz',   baz: string  }
   *   | { tag: 'quux', quux: boolean }
   *
   * pipe(
   *   foo,
   *   match.extract('bar'),
   * ) // Option<{ tag: 'bar', bar: number }>
   *
   * pipe(
   *   foo,
   *   match.extract('bar', 'baz'),
   * ) // Option<{ tag: 'bar', bar: number } | { tag: 'baz', baz: string  }>
   *
   */
  extract: <TU extends TaggedOnKey<K>, KUS extends Array<TU[K]>>(
    ...kus: KUS
  ) => (
    tu: TU,
  ) => O.Option<TaggedUnionMember<TU, K, KUS extends Array<TU[K]> ? KUS[number] : never>>
}

interface MatchOn {
  on: <K extends string>(key: K) => Match<K>
}

const matchOnKey = <K extends string>(key: K): Match<K> =>
  Object.assign(
    <TaggedUnion extends TaggedOnKey<K>, Z>(matchObj: Matchers<K, TaggedUnion, Z>) =>
      (tu: TaggedUnion): Z =>
        matchObj[tu[key]](tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>),
    {
      i: inverted(key),
      p: partial(key),
      w: widened(key),
      ip: invertedPartial(key),
      iw: invertedWidened(key),
      pw: partialWidened(key),
      ipw: invertedPartialWidened(key),
      extract: extract(key),
    },
  )

export const match: Match<"tag"> & MatchOn = Object.assign(matchOnKey("tag"), {
  on: matchOnKey,
})

export const matchS =
  <S extends string>(s: S) =>
  <Out>(matchObj: { [M in S]: () => Out }): Out =>
    matchObj[s]()

export const matchSI =
  <S extends string, Out>(matchObj: { [M in S]: () => Out }) =>
  (s: S): Out =>
    matchObj[s]()

function inverted<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>>(tu: TaggedUnion) =>
    <Z>(matchObj: Matchers<K, TaggedUnion, Z>): Z =>
      matchObj[tu[key]](tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>)
}

function partial<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>, Z>(
      matchObj: StrictPartial<Matchers<K, TaggedUnion, Z>>,
      otherwise?: () => Z,
    ) =>
    (tu: TaggedUnion) => {
      const matchedTag = tu[key]
      const f: false | ((tu_: TaggedUnionMember<TaggedUnion, K, typeof tu[K]>) => Z) =
        matchedTag in (matchObj as object) && matchObj[matchedTag]
      if (f !== false) {
        const ret = f(tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>)
        return otherwise ? ret : O.some(ret)
      }
      return otherwise ? otherwise() : O.none
    }
}

function widened<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>>(matchObj: Matchers<K, TaggedUnion, unknown>) =>
    (tu: TaggedUnion): PropReturns<typeof matchObj> =>
      matchObj[tu[key]](tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>) as PropReturns<
        typeof matchObj
      >
}

function invertedPartial<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>>(tu: TaggedUnion) =>
    <Z>(matchObj: StrictPartial<Matchers<K, TaggedUnion, Z>>, otherwise?: () => Z) =>
      otherwise ? matchOnKey(key).p(matchObj, otherwise)(tu) : matchOnKey(key).p(matchObj)(tu)
}

function invertedWidened<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>>(tu: TaggedUnion) =>
    <Z>(matchObj: Matchers<K, TaggedUnion, Z>) =>
      matchObj[tu[key]](tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>) as PropReturns<
        typeof matchObj
      >
}

function partialWidened<K extends string>(key: K) {
  return <
      TaggedUnion extends TaggedOnKey<K>,
      Z,
      Ms extends StrictPartial<Matchers<K, TaggedUnion, unknown>>,
    >(
      matchObj: Ms,
      otherwise?: () => Z,
    ) =>
    (tu: TaggedUnion) => {
      const matchedTag = tu[key]
      const f: false | ((tu_: TaggedUnionMember<TaggedUnion, K, typeof tu[K]>) => PropReturns<Ms>) =
        matchedTag in matchObj && matchObj[matchedTag]
      if (f !== false) {
        const ret = f(tu as TaggedUnionMember<TaggedUnion, K, typeof tu[K]>)
        return otherwise ? ret : O.some(ret)
      }
      return otherwise ? otherwise() : O.none
    }
}

function invertedPartialWidened<K extends string>(key: K) {
  return <TaggedUnion extends TaggedOnKey<K>>(tu: TaggedUnion) =>
    <Z, Ms extends StrictPartial<Matchers<K, TaggedUnion, unknown>>>(
      matchObj: Ms,
      otherwise?: () => Z,
    ) =>
      otherwise ? matchOnKey(key).pw(matchObj, otherwise)(tu) : matchOnKey(key).pw(matchObj)(tu)
}

function extract<K extends string>(key: K) {
  return <TU extends TaggedOnKey<K>, KUS extends Array<TU[K]>>(...kus: KUS) =>
    (tu: TU) =>
      pipe(
        kus,
        RA.findFirstMap((k) =>
          tu[key] === k
            ? O.some(tu as TaggedUnionMember<TU, K, KUS extends Array<TU[K]> ? KUS[number] : never>)
            : O.none,
        ),
      )
}
